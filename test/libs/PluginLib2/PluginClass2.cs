﻿using InterfaceLib;
using System;

namespace PluginLib2
{
    public class PluginClass2 : PluginInterface
    {
        public override string Name => "第二个插件";

        public override Version Version => Version.Parse("2.0.0");
        public override void Load()
        {
            Console.WriteLine("Load" + this.GetType().FullName);
        }
        public override string Method1()
        {
            return "这是从第二个插件里面来的数据";
        }
    }
}
